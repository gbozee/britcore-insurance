# britcore-insurance

My solution to Britcore's Product Development Hiring Project

## Problem Summary

To come up with a solution that makes it possible for insurers to define their own custom data model for their risks types, without maintaining hard coded database instances. Instead, insurers should be able to create their own risk types and attach as many different fields as they would like.

## Getting Started
After cloning the project, the following is required to get a local version up and running

```bash
$ python -m venv venv
$ source venv/bin/activate # windows users venv/Scripts/activate
$ pip install -r requirements/local.txt
# Assuming docker is installed, docker-compose could be use to provision a database
$ docker-compose up -d db
$ export DATABASE_URL=postgres://britcore:britcore@localhost:5522/britcore
$ pytest risks/tests.py # To run the tests
$ python manage.py migrate # run all migrations
$ python manage.py runserver # starts the development server
```

## Approach

### Backend
My model layer consisted of the following tables

1. [RiskType](./risks/models.py#RiskType): _Representation of an insurer risk type e.g automobile, housing e.t.c_
2. [RiskField](./risks/models.py#RiskField): _The fields to be attached to a risk type e.g in the case of an automobile risk type, some fields could be `car make`, `model`, etc._
3. [Submission](./risks/models.py#Submission): _After a risk type instance has been created and populated with fields, records matching the risk type, are saved here._

![Entity Diagram](./entity-diagram.svg "Entity Diagram")

**Entity diagram displaying relationship accross models _(generated with graph_models from `django-extension`)_**

A test driven approach was used in figuring out what a good REST API interface would look like. The endpoints implemented in this solution are

1. [Endpoint to fetch the list of all created risk types](./risks/tests.py#get_list)

```json
GET /risk-types/

Response:
{
    ...
    "result":[
        {
            "id": 1,
            "name": "Automobile",
            "fields":[
                {
                    "name":"Color",
                    ...
                }
            ]
        }
    ]
}
```

2. [Endpoint to fetch a single risk type by id](./risks/tests.py#single_risk_type)

```json
GET /risk-types/<pk>/

Response:
 {
    "id": 1,
    "name": "Automobile",
    "fields":[
        {
            "name":"Color",
            ...
        }
    ]
}
```

3. [Endpoint to create a risk type](./risks/tests.py#create_risk)

```json
POST /risk-types/

Body:
{
    "name":"Automobile"
    "fields":[
        {
            "name": "Color",
            "required": true,
            "type": "enum",
            "choices": ["Red", "Blue", "Yellow"]
        },
        {
            "name": "Email",
            "required": true,
            "type": "text",
            "additionalProps": {"kind": "email"}
        }
    ]
}

Response:
{
    "data": {
    "id": 1,
    "name": "Automobile Insurance",
    "fields": [
      {
        "name": "Color",
        "slug": "color",
        "type": "enum",
        "additionalProps": {
          "choices": [
            "Red",
            "Blue",
            "Yellow"
          ]
        },
        "required": true
      },
      {
        "name": "Email",
        "slug": "email",
        "type": "text",
        "additionalProps": {
          "kind": "email"
        },
        "required": true
      }
    ]
  }
}
```

4. [Endpoint to update an existing risk type by id](./risks/tests.py#update_risk)

```json
PUT /risk-types/<id>/

Body:
{
    "name": "Automobile Insurance",
	"delete_fields": [ "color", "model" ],
	"update_fields": {
            "email": {
                "required": true,
		"additionalProps": {
	            "kind": "email",
		    "placeholder": "Last name of the user"
		}
	  }
	},
	"new_fields": [
	  {
	    "name": "First name",
	    "type": "text",
	    "required": true,
	    "additionalProps": {
	      "placeholder": "First name of the user"
		}
	  }
	]
}

Response:
{
    "data": {
    "id": 1,
    "name": "Automobile Insurance",
    "fields": [
      {
        "name": "First name",
        "slug": "first-name",
        "type": "text",
        "additionalProps": {
          "placeholder": "First name of the user"
        },
        "required": true
      },
      ...
      {
        "name": "Email",
        "slug": "email",
        "type": "text",
        "additionalProps": {
          "kind": "email"
        },
        "required": true
      }
    ]
  }
}
```
5. [Endpoint to get and post submissions for a particular risk-type](./risks/tests.py#submissions)

### Frontend
The frontend was built with VueJS and styled using Boostrap. A single Vue model instance with 3 Vue components were created.

*In order to avoid adding complexity to the project, I opted not to use the vue-loader setup but instead the vuejs build that didn't require a build step.*

The ViewModel consisted of the helper methods `fetchData` in making a web api call to fetch the `risk-types`. This was then loaded in the `created` lifecycle.

Two Vue components were created to handle the supported risk types. `enum-field` for handling `enum` field types and `input-field` for handling `text`, 'date`, and `number` field types.


## Deployment

AWS Cloudformation templates where created [here](./deploy-aws/stack.yml), to provision an RDS Postgres instance as well as configuring S3.

```bash
$ sudo apt-get install awscli
$ aws configure # configure aws with credentials.
$ cd <project_directory>/deploy-aws
$ aws cloudformation update-stack --stack-name britcore-app --template-body file://$PWD/deploy-aws/stack.yml
````
Zappa was used in deploying the project.

Gitlab CI Piplines was used in scripting the deployment of the project to AWS. The configuration is located in [Gitlab CI config file.](./.gitlab-ci.yml). Once the project is pushed to the repository, it is automatically deployed.

A running version of the project is hosted [here](https://flho8brga8.execute-api.us-east-1.amazonaws.com/dev)

