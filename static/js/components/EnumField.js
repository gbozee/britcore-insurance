Vue.component("EnumField", {
  template: `
    <div class="form-group" : key="field.id">
    <label class="col-sm-2 col-form-label" : for="field.slug">{{ field.label }}</label>
    <div class="col-sm-10">
        <select class="form-control" : value="value" @change="$emit('input',$event.target.value)" v-bind="field">
        <option : value="undefined">{{ defaultText(field) }}</option>
    <option v-for="item in field.options" : key="item">{{ item }}</option>
      </select >
    <div v-if="hasError" class="invalid-feedback">{{ errorMessage(field) }}</div>
    </div>
  </div>

    `,
  props: ["field", "type", "value", "displayError"],
  computed: {
    hasError() {
      let { required = false } = this.field;
      return this.displayError && required && !Boolean(this.value);
    }
  },
  methods: {
    isTextFieldtype({ additionalProps = { kind: "text" } }) {
      return ["email", "text", "number"].includes(additionalProps.kind);
    },
    isTextArea({ additionalProps }) {
      return additionalProps.kind === "textarea";
    },
    defaultText({ additionalProps }) {
      let { defaultText = "Select" } = additionalProps;
      return defaultText;
    },
    errorMessage({ additionalProps }) {
      let { errorMessage = "This field is required" } = additionalProps;
      return errorMessage;
    }
  }
});

