Vue.component("Form", {
  props: ["form"],
  data:function() {
    return {
      values: {},
      show: false,
      displayError: false,
      loading: false,
      status: "",
      message: null
    };
  },
  computed: {
    result() {
      return JSON.stringify(this.values);
    }
  },
  methods: {
    onChange(event, field) {
      this.values[field.slug] = event.target.value;
    },
    onSubmit(event) {
      event.preventDefault();
      this.displayError = true;
      let fieldsToValidate = this.form.fields
        .filter(x => x.required)
        .map(x => x.slug);
      let validate = fieldsToValidate.every(x => Boolean(this.values[x]));
      if (validate) {
        this.show = true;
        this.loading = true;
        this.submitForm(this.values)
          .then(() => {
            this.loading = false;
            this.status = "success";
            this.message = "Form saved!!";
          })
          .catch(error => {
            this.loading = false;
            this.status = "error";
            this.message = "Error Occured!!";
          });
      }
    },
    submitForm(data, fail = false) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (fail) {
            reject();
          } else {
            resolve();
          }
        }, 3000);
      });
    },
    fieldType({ additionalProps }) {
      let { kind = "text" } = additionalProps;
      return kind;
    },
    defaultText({ additionalProps }) {
      let { defaultText = "Select" } = additionalProps;
      return defaultText;
    },
    isInputField(field) {
      return ["text", "date", "number", "enum"].includes(field.type);
    }
  },
  template: `
  <template>
      <div class="ml-5 mr-5">
        <h2>{{form.name}}</h2>
        <!-- <template v-if="show">{{result}}</template> -->
        <form @submit="onSubmit" :class="displayError && 'was-validated'" novalidate>
          <template v-for="field in form.fields">
            <div :key="field.slug">
              <template v-if="field.type==='text'">
                <InputField
                  v-model="values[field.slug]"
                  :type="fieldType(field)"
                  :field="field"
                  :displayError="displayError"
                ></InputField>
              </template>
              <template v-if="field.type==='number'">
                <InputField
                  v-model="values[field.slug]"
                  :type="field.type"
                  :field="field"
                  :displayError="displayError"
                ></InputField>
              </template>
              <template v-if="field.type==='date'">
                <InputField
                  v-model="values[field.slug]"
                  :type="field.type"
                  :field="field"
                  :displayError="displayError"
                ></InputField>
              </template>
              <template v-if="field.type==='enum'">
                <EnumField
                  v-model="values[field.slug]"
                  :type="field.type"
                  :field="field"
                  :displayError="displayError"
                ></EnumField>
              </template>
            </div>
          </template>
          <div class="form-footer">
            <button type="submit" :disabled="loading" class="btn btn-lg btn-primary">Submit form</button>
            <div v-if="message" :class="\`alert alert-\${status}\`" role="alert">{{message}}</div>
          </div>
        </form>
      </div>
    </template>
    `
});

