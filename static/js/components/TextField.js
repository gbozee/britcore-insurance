Vue.component("TextField", {
  template: `
  <div class="form-group" :key="field.id">
    <label class="col-sm-2 col-form-label" :for="field.slug">{{field.label}}</label>
    <div class="col-sm-10">
      <textarea
        class="form-control"
        v-if="isTextArea(field)"
        :value="value"
        :name="field.slug"
        :id="field.slug"
        v-bind="field.additionalProps"
        :required="field.required"
        @input="$emit('input', $event.target.value)"
      ></textarea>
      <input
        v-else
        :id="field.slug"
        :name="field.slug"
        class="form-control"
        :required="field.required"
        :value="value"
        @input="$emit('input', $event.target.value)"
        :type="type"
        v-bind="field.additionalProps"
      >
      <div v-if="hasError" class="invalid-feedback">{{errorMessage(field)}}</div>
    </div>
  </div>
`,
  props: ["field", "type", "value", "displayError"],
  computed: {
    hasError() {
      let { required = false } = this.field;
      return this.displayError && required && !Boolean(this.value);
    }
  },
  methods: {
    isTextFieldtype({ additionalProps = { kind: "text" } }) {
      console.log(additionalProps);
      return ["email", "text", "number"].includes(additionalProps.kind);
    },
    isTextArea({ additionalProps }) {
      return additionalProps.kind === "textarea";
    },
    errorMessage({ additionalProps }) {
      let { errorMessage = "This field is required" } = additionalProps;
      return errorMessage;
    }
  }
});

