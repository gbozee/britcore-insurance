Vue.component("input-field", {
  template: `
  <div class="form-group" :key="field.id">
    <label class="col-sm-2 col-form-label" :for="field.slug">{{getLabel(field)}}</label>
    <div class="col-sm-10">
      <textarea
        class="form-control"
        v-if="isTextArea(field)"
        :value="value"
        :name="field.slug"
        :id="field.slug"
        v-bind="field.additionalProps"
        :required="field.required"
        @input="$emit('input', $event.target.value)"
      ></textarea>
      <input
        v-else
        :id="field.slug"
        :name="field.slug"
        class="form-control"
        :required="field.required"
        :value="value"
        @input="$emit('input', $event.target.value)"
        :type="type"
        v-bind="field.additionalProps"
      >
      <div v-if="hasError" class="invalid-feedback">{{errorMessage(field)}}</div>
    </div>
  </div>
`,
  props: ["field", "type", "value", "displayError"],
  computed: {
    hasError() {
      let { required = false } = this.field;
      return this.displayError && required && !Boolean(this.value);
    }
  },
  methods: {
    isTextFieldtype({ additionalProps = { kind: "text" } }) {
      console.log(additionalProps);
      return ["email", "text", "number"].includes(additionalProps.kind);
    },
    isTextArea({ additionalProps }) {
      return additionalProps.kind === "textarea";
    },
    errorMessage({ additionalProps }) {
      let { errorMessage = "This field is required" } = additionalProps;
      return errorMessage;
    },
    getLabel({ additionalProps, name }) {
      let { label = name } = additionalProps;
      return label;
    }
  }
});

Vue.component("enum-field", {
  template: `
    <div class="form-group" :key="field.id">
    <label class="col-sm-2 col-form-label" :for="field.slug">{{ getLabel(field) }}</label>
    <div class="col-sm-10">
        <select class="form-control" :value="value" @change="$emit('input',$event.target.value)" v-bind="field">
        <option :value="undefined">{{ defaultText(field) }}</option>
    <option v-for="item in getOptions(field)" :key="item">{{ item }}</option>
      </select >
    <div v-if="hasError" class="invalid-feedback">{{ errorMessage(field) }}</div>
    </div>
  </div>

    `,
  props: ["field", "type", "value", "displayError"],
  computed: {
    hasError() {
      let { required = false } = this.field;
      return this.displayError && required && !Boolean(this.value);
    }
  },
  methods: {
    isTextFieldtype({ additionalProps = { kind: "text" } }) {
      return ["email", "text", "number"].includes(additionalProps.kind);
    },
    isTextArea({ additionalProps }) {
      return additionalProps.kind === "textarea";
    },
    defaultText({ additionalProps }) {
      let { defaultText = "Select" } = additionalProps;
      return defaultText;
    },
    errorMessage({ additionalProps }) {
      let { errorMessage = "This field is required" } = additionalProps;
      return errorMessage;
    },
    getOptions({ additionalProps }) {
      let { choices = [] } = additionalProps;
      return choices;
    },
    getLabel({ additionalProps, name }) {
      let { label = name } = additionalProps;
      return label;
    }
  }
});

Vue.component("b-form", {
  props: ["form","url"],
  data: function() {
    return {
      values: {},
      show: false,
      displayError: false,
      loading: false,
      status: "",
      message: null
    };
  },
  computed: {
    result() {
      return JSON.stringify(this.values);
    }
  },
  methods: {
    onChange(event, field) {
      this.values[field.slug] = event.target.value;
    },
    onSubmit(event) {
      event.preventDefault();
      this.displayError = true;
      let fieldsToValidate = this.form.fields
        .filter(x => x.required)
        .map(x => x.slug);
      let validate = fieldsToValidate.every(x => Boolean(this.values[x]));
      if (validate) {
        this.show = true;
        this.loading = true;
        postData(`${this.url}risk-types/${this.form.id}/submissions/`, this.values)
          .then(result => {
            this.loading = false;
            this.status = "success";
            this.message = "Form saved!!";
          })
          .catch(error => {
            this.loading = false;
            this.status = "error";
            this.message = "Error Occured!!";
          });
      }
    },
    fieldType({ additionalProps }) {
      let { kind = "text" } = additionalProps;
      return kind;
    },
    defaultText({ additionalProps }) {
      let { defaultText = "Select" } = additionalProps;
      return defaultText;
    },
    isInputField(field) {
      return ["text", "date", "number", "enum"].includes(field.type);
    }
  },
  template: `
      <div class="ml-5 mr-5">
        <h2>{{form.name}}</h2>
        <!-- <template v-if="show">{{result}}</template> -->
        <form @submit="onSubmit" :class="displayError && 'was-validated'" novalidate>
          <template v-for="field in form.fields">
            <div :key="field.slug">
              <template v-if="field.type==='text'">
                <input-field
                  v-model="values[field.slug]"
                  :type="fieldType(field)"
                  :field="field"
                  :displayError="displayError"
                ></input-field>
              </template>
              <template v-if="field.type==='number'">
                <input-field
                  v-model="values[field.slug]"
                  :type="field.type"
                  :field="field"
                  :displayError="displayError"
                ></input-field>
              </template>
              <template v-if="field.type==='date'">
                <input-field
                  v-model="values[field.slug]"
                  :type="field.type"
                  :field="field"
                  :displayError="displayError"
                ></input-field>
              </template>
              <template v-if="field.type==='enum'">
                <enum-field
                  v-model="values[field.slug]"
                  :type="field.type"
                  :field="field"
                  :displayError="displayError"
                ></enum-field>
              </template>
            </div>
          </template>
          <div class="form-footer">
            <button type="submit" :disabled="loading" class="btn btn-lg btn-primary">Submit form</button>
            <div v-if="message" :class="\`alert alert-\${status}\`" role="alert">{{message}}</div>
          </div>
        </form>
      </div>
    `
});


function postData(url = ``, data = {}) {
  // Default options are marked with *
  return fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    mode: "cors", // no-cors, cors, *same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "same-origin", // include, *same-origin, omit
    headers: {
      "Content-Type": "application/json"
      // "Content-Type": "application/x-www-form-urlencoded",
    },
    redirect: "follow", // manual, *follow, error
    referrer: "no-referrer", // no-referrer, *client
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  }).then(response => response.json()); // parses JSON response into native Javascript objects
}
