import json

import pytest

from risks import models, serializers


def create_sample_risks():
    first_name = models.RiskField.objects.create(
        name="First name", type=models.RiskField.TEXT, required=False
    )
    last_name = models.RiskField.objects.create(
        name="Last name",
        type=models.RiskField.TEXT,
        other_details={"placeholder": "Your last name."},
    )
    gender = models.RiskField.objects.create(
        name="Gender",
        type=models.RiskField.ENUM,
        other_details={"choices": ["Male", "Female", "Others"]},
        required=True,
    )
    home_issurance = models.RiskType.objects.create(name="Home Insurance")
    home_issurance.fields.add(first_name)
    home_issurance.fields.add(last_name)
    home_issurance.fields.add(gender)
    return home_issurance


def result_response(record):
    return {
        "id": record.pk,
        "name": "Home Insurance",
        "fields": [
            {
                "name": "First name",
                "additionalProps": {},
                "required": False,
                "slug": "first-name",
                "type": "text",
            },
            {
                "name": "Last name",
                "additionalProps": {"placeholder": "Your last name."},
                "required": True,
                "slug": "last-name",
                "type": "text",
            },
            {
                "name": "Gender",
                "additionalProps": {"choices": ["Male", "Female", "Others"]},
                "required": True,
                "slug": "gender",
                "type": "enum",
            },
        ],
    }


# Create your tests here.
# <a href="#single_risk_type">Single Risk Type by id</a>
@pytest.mark.django_db
def test_single_risk_type_endpoint(client):
    record = create_sample_risks()
    response = client.get(f"/risk-types/{record.pk}/", content_type="application/json")
    assert response.status_code == 200
    assert response.json() == result_response(record)

# <a href="#get_list">Get List of Risk Types</a>
@pytest.mark.django_db
def test_list_of_risk_type_endpoint(client):
    response = client.get("/risk-types/", content_type="application/json")
    assert response.status_code == 200
    result = response.json()
    assert result["results"] == []

    # populate db with test data
    record = create_sample_risks()
    response = client.get("/risk-types/", content_type="application/json")
    assert response.status_code == 200
    result = response.json()
    assert result["results"] == [result_response(record)]

# <a href="#create_risk">Create Risk Type</a>
@pytest.mark.django_db
def test_create_risk_type_endpoint(client):
    post_data = {
        "name": "Automobile Insurance",
        "fields": [
            {
                "name": "Color",
                "required": True,
                "type": "enum",
                "options": ["Red", "Blue", "Yellow"],
            },
            {"name": "Model", "required": True, "type": "text"},
            {
                "name": "Email",
                "required": True,
                "type": "text",
                "additionalProps": {"kind": "email"},
            },
        ],
    }
    response = client.post(
        "/risk-types/", data=json.dumps(post_data), content_type="application/json"
    )
    assert response.status_code == 201
    data = response.json()["data"]
    assert data == {
        "id": data["id"],
        "name": "Automobile Insurance",
        "fields": [
            {
                "name": "Color",
                "additionalProps": {"choices": ["Red", "Blue", "Yellow"]},
                "required": True,
                "slug": "color",
                "type": "enum",
            },
            {
                "name": "Model",
                "additionalProps": {},
                "required": True,
                "slug": "model",
                "type": "text",
            },
            {
                "name": "Email",
                "additionalProps": {"kind": "email"},
                "required": True,
                "slug": "email",
                "type": "text",
            },
        ],
    }

# <a href="#update_risk">Update Existing Risk type</a>
@pytest.mark.django_db
def test_update_risk_type_endpoint(client):
    record = create_sample_risks()
    assert record.field_data("gender").required
    assert record.field_data("last-name") is not None
    put_data = {
        "name": "Car Insurance",
        "update_fields": {"gender": {"required": False}},
        "delete_fields": ["last-name"],
        "new_fields": [
            {
                "name": "Email",
                "required": True,
                "type": "text",
                "additionalProps": {"kind": "email"},
            }
        ],
    }
    response = client.put(
        f"/risk-types/{record.pk}/",
        data=json.dumps(put_data),
        content_type="application/json",
    )
    assert response.status_code == 201
    data = response.json()["data"]
    assert data == {
        "id": record.pk,
        "name": "Car Insurance",
        "fields": [
            {
                "name": "First name",
                "additionalProps": {},
                "required": False,
                "slug": "first-name",
                "type": "text",
            },
            {
                "name": "Email",
                "additionalProps": {"kind": "email"},
                "required": True,
                "slug": "email",
                "type": "text",
            },
            {
                "name": "Gender",
                "additionalProps": {"choices": ["Male", "Female", "Others"]},
                "required": False,
                "slug": "gender",
                "type": "enum",
            },
        ],
    }

#<a href="#submissions">GET or Post submissions</a>
@pytest.mark.django_db
def test_submission_of_form(client):
    instance = create_sample_risks()
    data = {"first-name": "John", "last-name": "Doe", "gender": "Male"}
    response = client.post(
        f"/risk-types/{instance.pk}/submissions/",
        data=json.dumps(data),
        content_type="application/json",
    )
    assert response.status_code == 201
    result = response.json()["data"]
    assert result["result"] == data
    assert result["id"] is not None
    response = client.get(
        f"/risk-types/{instance.pk}/submissions/", content_type="application/json"
    )
    assert response.status_code == 200
    assert response.json()["result"] == [result]

