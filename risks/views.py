from django.conf import settings
from django.views.generic import TemplateView
from rest_framework import viewsets, status
from rest_framework.views import Response
from rest_framework.decorators import action
from . import models, serializers


# Create your views here.
class RiskTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = models.RiskType.objects.all()
    serializer_class = serializers.RiskTypeSerializer

    def create(self, request):
        data = request.data
        fields = data.pop("fields", {})
        form = serializers.RiskTypeCreateForm(data)
        field_post_data = serializers.RiskFieldFormset.build_post_data(fields)
        formset = serializers.RiskFieldFormset(field_post_data)
        if formset.is_valid() and form.is_valid():
            form_instance = form.save_form_with_fields(formset)
            record = serializers.RiskTypeSerializer(form_instance).data
            return Response({"data": record}, status=201)
        return Response(
            {"errors": {"fields": formset.errors, "form": form.errors}}, status=400
        )

    def update(self, request, pk=None):
        data = request.data
        update_fields = data.pop("update_fields", {})
        delete_fields = data.pop("delete_fields", [])
        new_fields = data.pop("new_fields", [])
        instance = models.RiskType.objects.filter(pk=pk).first()
        created_fields = []
        if new_fields:
            field_post_data = serializers.RiskFieldFormset.build_post_data(new_fields)
            formset = serializers.RiskFieldFormset(field_post_data)
            if formset.is_valid():
                created_fields = formset.save()
        if instance:
            form = serializers.RiskTypeCreateForm(data, instance=instance)
            if form.is_valid():
                record = form.update_fields(
                    update_fields, delete_fields, created_fields
                )
                serialized_record = serializers.RiskTypeSerializer(record).data
                return Response({"data": serialized_record}, status=201)
            return Response({"errors": form.errors}, status=400)
        return Response({"message": "Not found"}, status=400)

    @action(detail=True, methods=["post", "get"])
    def submissions(self, request, pk=None):
        instance = self.get_object()
        if request.method == "GET":
            result = serializers.SubmissionSerializer(
                instance.submissions.all(), many=True
            ).data
            return Response({"result": result}, status=status.HTTP_200_OK)
        result = models.Submission.objects.create(
            risk_type=instance, result=request.data
        )
        record = serializers.SubmissionSerializer(result).data
        return Response({"data": record}, status=status.HTTP_201_CREATED)


class AppView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        url = self.request.build_absolute_uri("/")
        if "/dev" not in url:
            url += "dev/"
        return {"DEBUG": settings.DEBUG, "URL":url}
