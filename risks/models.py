import json

from autoslug import AutoSlugField
from django.db import models
from django.contrib.postgres.fields import JSONField

# Create your models here.


# <a href="#RiskField">RiskField</a>
class RiskField(models.Model):
    TEXT = "text"
    DATE = "date"
    NUMBER = "number"
    ENUM = "enum"
    CHOICES = ((TEXT, "text"), (DATE, "data"), (NUMBER, "number"), (ENUM, "enum"))
    name = models.CharField(max_length=80)
    slug = AutoSlugField(populate_from="name")
    required = models.BooleanField(default=True)
    other_details = JSONField(default=dict)
    type = models.CharField(max_length=20, choices=CHOICES)

    def __repr__(self):
        return f"<RiskField: {self.name} {self.type}>"

    def __str__(self):
        return self.name

# <a href="#RiskType">RiskType</a>
class RiskType(models.Model):
    name = models.CharField(max_length=50, db_index=True)
    fields = models.ManyToManyField(RiskField)
    slug = AutoSlugField(populate_from="name")

    def __repr__(self):
        return f"<RiskType: {self.slug}>"

    def __str__(self):
        return self.name

    def field_data(self, slug):
        return self.fields.filter(slug=slug).first()

# <a href="#Submission">Submission</a>
class Submission(models.Model):
    risk_type = models.ForeignKey(
        RiskType, null=True, on_delete=models.CASCADE, related_name="submissions"
    )
    result = JSONField(default=dict)
