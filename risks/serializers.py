from django import forms
from django.contrib.postgres.forms import JSONField
from rest_framework import serializers

from .models import RiskField, RiskType, Submission


class SubmissionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Submission
        fields = ["id", "result"]


class RiskFieldSerializer(serializers.HyperlinkedModelSerializer):
    additionalProps = serializers.JSONField(source="other_details")

    class Meta:
        model = RiskField
        fields = ["name", "slug", "type", "additionalProps", "required"]


class RiskTypeSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.CharField()
    fields = RiskFieldSerializer(many=True)

    class Meta:
        model = RiskType
        fields = ("id", "name", "fields")


class RiskTypeCreateForm(forms.ModelForm):
    class Meta:
        model = RiskType
        fields = ["name"]

    def save_form_with_fields(self, formset):
        field_instances = formset.save()
        form_instance = self.save()
        for _field in field_instances:
            form_instance.fields.add(_field)
        return form_instance

    def update_fields(self, update_fields, delete_fields, created_fields):
        for key, value in update_fields.items():
            field_instance = self.instance.field_data(key)
            for k, v in value.items():
                setattr(field_instance, k, v)
                field_instance.save()
        for key in delete_fields:
            field_instance = self.instance.field_data(key)
            field_instance.delete()
        for field in created_fields:
            self.instance.fields.add(field)
        return self.save()


class RiskFieldForm(forms.ModelForm):
    additionalProps = JSONField(required=False)
    options = JSONField(required=False)

    class Meta:
        model = RiskField
        fields = ["name", "required", "type"]

    def save(self, commit=False):
        instance = super().save(commit=False)
        choices = self.cleaned_data.pop("options", None)
        instance.other_details = self.cleaned_data.pop("additionalProps", None) or {}
        if choices:
            instance.other_details = {**instance.other_details, "choices": choices}
        if commit:
            instance.save()
        return instance


def build_form_dict(field, index):
    return {f"form-{index}-{key}": value for key, value in field.items()}


class RiskFormSet(forms.BaseModelFormSet):
    @classmethod
    def build_post_data(cls, post_data):
        length = len(post_data)
        form_dict_array = [build_form_dict(x, i) for i, x in enumerate(post_data)]
        as_dict = {}
        for i in form_dict_array:
            as_dict = {**as_dict, **i}
        return {
            "form-TOTAL_FORMS": length,
            "form-MAX_NUM_FORMS": "",
            "form-INITIAL_FORMS": "0",
            **as_dict,
        }


RiskFieldFormset = forms.modelformset_factory(
    RiskField, form=RiskFieldForm, formset=RiskFormSet
)
