from .dev import *

ALLOWED_HOSTS = ["flho8brga8.execute-api.us-east-1.amazonaws.com",'localhost']

INSTALLED_APPS += ["django_s3_storage"]

YOUR_S3_BUCKET = "britcore-z"

STATICFILES_STORAGE = "django_s3_storage.storage.StaticS3Storage"
AWS_S3_BUCKET_NAME_STATIC = YOUR_S3_BUCKET

# These next two lines will serve the static files directly 
# from the s3 bucket
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % YOUR_S3_BUCKET
STATIC_URL = "https://%s/" % AWS_S3_CUSTOM_DOMAIN
